#!/bin/bash
if [ ! -f ./container.sif ]; then
	# build docker environment
	docker build -t rds-build-env ./scripts/
	# export docker container
	docker save --format oci-archive --output container.tar rds-build-env
	# build singularity container
	singularity build --fakeroot ./container.sif ./scripts/singularityfile.def
	rm ./container.tar
fi
# compile RDS
singularity run --containall -B $PWD/src:/build/src ./container.sif
