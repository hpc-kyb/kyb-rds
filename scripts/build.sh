#!/bin/bash
set -ex
cd /build/src
if [[ ! -d "yarraclient" ]]
then
    #git clone https://bitbucket.org/yarra-dev/yarraclient.git
    git clone https://gitlab+deploy-token-7:tCw_FxVYSnjk_FSEsfuT@gitlab.tuebingen.mpg.de/dbosch/yarraclient
    cd yarraclient
    git checkout remoteLPFI
fi

# create directory for compiled exe files. Clear all old contents.
if [[ ! -d "/build/src/release" ]]
then
    mkdir -p /build/src/release
fi
rm -rf /build/src/release/*

echo "Building RDS"
echo "======================="

cd /build/src/yarraclient/Client
/build/mxe/usr/i686-w64-mingw32.static/qt5/bin/qmake
make -j8
cp ./release/RDS.exe /build/src/release

echo "Building RaidSimulator"
echo "======================="

cd /build/src/yarraclient/Simulator
/build/mxe/usr/i686-w64-mingw32.static/qt5/bin/qmake
make -j8
cp ./release/RaidSimulator.exe /build/src/release
