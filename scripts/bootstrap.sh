#!/bin/bash
    mkdir -p /build && cd /build
    git clone https://github.com/mxe/mxe.git
    cd mxe
    # prepare build environment
    make qt5 --jobs=4 JOBS=2
    echo "export PATH=/build/mxe/usr/bin:$PATH" >> $HOME/.bashrc

