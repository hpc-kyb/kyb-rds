#!/bin/bash

set -ex
currP=$(pwd)
#podman unshare rm -rf "${currP}/build"
if [[ ! -d "src" ]]
then
    mkdir -p "${currP}/src"
fi
podman build -t rds-build-env ./scripts/
podman run --rm -it -v $(pwd)/src:/build/src:rw -w /build rds-build-env ./build.sh





